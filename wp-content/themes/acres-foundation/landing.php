<?php /* Template Name: landing page */ ?>
<?php get_header(); ?>

<section class="max-width-ct mb-space">
	<div class="container-fluid">
		<div class="row bg-landing">
		<div class="col-md-7 p-0">
			<a href="#landingform" class="headtag">
				<h2>Planning to open a new school?</h2>
				<h3>Collaborate with us</h3>
				<p>Fill up the enquiry form and our team shall contact you! <img src="<?php echo get_template_directory_uri()  ?>/assets/images/right-arrow.svg"></p>
			</a>

			<a href="#landingform" class="headtag">
				<h2>Wish to take your existing school from Good to GREAT?</h2>
				<h3>Collaborate with us</h3>
				<p>Fill up the enquiry form and our team shall contact you! <img src="<?php echo get_template_directory_uri()  ?>/assets/images/right-arrow.svg"></p>
			</a>

			<a href="<?php echo site_url('/home'); ?>" class="gotohome">Go to Website</a>
		</div>

		<div class="col-md-5 submit-btn" id="landingform">
			<div class="landform">
				<?php  gravity_form( 3, false, false, false, '', true, 12 );?>
			</div>
		</div>

		</div>
	</div>
</section>

<!-- <div id="popup1" class="overlay onloadpop">
	<div class="popup">
		<h2>Connect with us <br>to explore Educational</h2>
		<p>Enter your details below</p>
		<a class="close" href="#">&times;</a>
		<div class="content">
			Thank to pop me out of that button, but now i'm done so you can close this window.
		  	<?php  //gravity_form( 1, false, false, false, '', true, 12 );?>
		</div>
	</div>
</div> -->

<script>
document.getElementById('gform_3').onsubmit = function(){
	console.log("U clicked on submit") ;

	var validation_error = document.getElementsByClassName('validation_error');
	if (validation_error.length > 0) {
		console.log('Validation length > 0 ') ; 
	}
	else 
	{
		console.log('NOT FOUND') ;
	}
		
}
</script>

<?php get_footer(); ?>