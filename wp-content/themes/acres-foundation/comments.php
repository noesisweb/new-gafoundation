<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Acres_Foundation
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'acres-foundation' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().



	$args = array(
		'fields' => apply_filters(
			'', array(
				'author' =>'<p class="comment-form-author">' . '<input required id="author" placeholder="Name *" name="author" type="text" value="' .
					esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />'.
					'<label for="author">' . __( '' ) . '</label> ' .
					( $req ? '<span class="required">*</span>' : '' )  .
					'</p>'
					,
				'email'  => '<p class="comment-form-email">' . '<input required id="email" placeholder="Email Address *" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
					'" size="30"' . $aria_req . ' />'  .
					'<label for="email">' . __( '' ) . '</label> ' .
					( $req ? '<span class="required">*</span>' : '' ) .
					'</p>'
			)
		),
		'comment_field' => '<p class="comment-form-comment">' .
			'<textarea id="comment" name="comment" placeholder="Write your comment here *" cols="50" rows="8" aria-required="true"></textarea>' .
			'</p>',
		'comment_notes_after' => '',
		'title_reply' => '<div class="crunchify-text"><h5>Leave a comment</h5></div>'
	);


	
	comment_form($args);
	?>

</div><!-- #comments -->



<script>

jQuery('document').ready(function($){
	var commentform=$('#commentform'); // find the comment form
    commentform.prepend('<div id="comment-status" class="col-12" ></div>'); // add info panel before the form to provide feedback or errors
    var statusdiv=$('#comment-status'); // define the infopanel

    commentform.submit(function(){
	    //serialize and store form data in a variable
	    
	    var email = $("#commentform").find("#email").val();
	    var author = $("#commentform").find("#author").val();
	    var comment = $("#commentform").find("#comment").val();
	    
	    if(email == ""){
		    statusdiv.html('<p class="ajax-error" >All fields required *</p>');
		    return false;
	    }else if(author == ""){
		    statusdiv.html('<p class="ajax-error" >All fields required *</p>');
		    return false;
	    }else if(comment == ""){
		    statusdiv.html('<p class="ajax-error" >All fields required *</p>');
		    return false;
	    }
        var formdata=commentform.serialize();
        //Add a status message
        //Extract action URL from commentform
        var formurl=commentform.attr('action');
        //Post Form with data
		//statusdiv.html('<p class="ajax-error" >You might have left one of the fields blank, or be posting too quickly</p>');
		//return false;
        $.ajax({
            type: 'post',
            url: formurl,
            data: formdata,
            success: function(data, textStatus){

                if(data == "success" || textStatus == "success"){
                    statusdiv.html('<p class="ajax-success" style="color:#008000">Thanks for your comment. We appreciate your response.</p>');
					console.log('You succeded in your comment') ;
                }else{
                    statusdiv.html('<p class="ajax-error" >Please wait a while before posting your next comment</p>');
                    commentform.find('textarea[name=comment]').val('');
                }
            }
        });
        return false;
    });
});

</script>
