<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Acres_Foundation
 */

?>

	
<footer>
	<section class="footer-one">
		<div class="container">
			<div class="row">
				<div class="col-md-4 first-row">
					<a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/logo.svg" alt=""></a>

					<!-- <ul>
						<li><a href="">HOME</a></li>
						<li><a href="">LEADERSHIP</a></li>
						<li><a href="">APPROACH</a></li>
						<li><a href="">PARTNERSHIPS</a></li>
						<li><a href="">BLOG</a></li>
					</ul> -->
					<?php
						wp_nav_menu( 
							array( 
							    'theme_location'	=>	'footer-menu', 
							    'container'			=>	'',
							    'menu_class'		=>	''
							    // 'walker'			=>	$walker 
							) 
						);
					?>
				</div>
				<div class="col-md-4 second-row">
					<h2>CONNECT</h2>
					<div class="icon-ct">
							<a href="https://www.facebook.com/theacresfoundation" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/fb.svg"></a>
							<a href="https://www.instagram.com/theacresfoundation/" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/inst.svg"></a>
							<a href="https://www.linkedin.com/in/the-acres-foundation-8128931a3" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/in.svg"></a>
						</div>
				</div>
				<div class="col-md-4 third-row">
					<h2>CONTACT</h2>
					<p><a href="mailto:enquiries@acresfoundation.org">ep.enquiries@acresfoundation.org</a></p>
					<p><a href="tel:+918591201990">+91 8591201990</a></p>
					<p><a href="tel:+918591201991">+91 8591201991</a></p>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row coppy-right">
				<div class="col-md-5 col-lg-4">
					<p>© 2020 THE ACRES FOUNDATION</p>
				</div>
				<div class="col-md-3 col-lg-4 second-row">
					<!-- <a href="">Privacy Policy</a> -->
				</div>
				<div class="col-md-4 col-lg-4">
					<!-- <p>Design and content: Spike Brand & Content Studio</p> -->
					<a href="<?php echo site_url('/privacy-policy/'); ?>">Privacy Policy</a>
				</div>
			</div>
		</div>
	</section>
</footer>

<div class="i-button">
	<a href="#popup1">
		<img src="<?php echo get_template_directory_uri()  ?>/assets/images/icon.png" alt="" class="img-fluid">
	</a>
</div>
<!-- <div id="popup1" class="overlay onloadpop"> -->
<div id="popup1" class="overlay">
	<div class="popup sticky-form-ct">
		<h2>Connect with us to explore <br>Educational Partnership opportunities</h2>
		<p>Enter your details below</p>
		<a class="close" href="#">&times;</a>
		<div class="content">
			<!-- Thank to pop me out of that button, but now i'm done so you can close this window. -->

			<?php if(SITE_ENV == 'dev_server'){ ?>
				<?php  gravity_form( 5, false, false, false, '', true, 12 );?>
			<?php }
			else if (SITE_ENV == 'production'){ ?>
				<?php  gravity_form( 8, false, false, false, '', true, 12 );?>
			<?php } ?>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
