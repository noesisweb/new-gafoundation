<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Acres_Foundation
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-193786526-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-193786526-2');
</script>

	
	<link rel="profile" href="https://gmpg.org/xfn/11">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M54WM7X');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '261241782288954');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=261241782288954&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<!-- Back to top button -->
<div class="back_to_top">
	<a id="btnscroll">
		<span class="toptxt">Top</span>
	</a>
	
</div>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M54WM7X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="navigation-wrap smallnav start-header start-style mt-md-4 mb-md-4">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 max-width-ct">
					<nav class="navbar navbar-expand-md navbar-light">
					
						<a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/logo.svg" alt=""></a>	
						
						<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topmenu" aria-controls="topmenu" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button> -->
						<button type="button" data-toggle="modal" data-target="#sidebar-right" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
						
						<div class="collapse navbar-collapse topmenu">
<!-- 							<ul class="navbar-nav ml-auto py-4 py-md-0"> -->
								<!-- <li class="nav-item pl-md-0 ml-0 active">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<a class="dropdown-item" href="#">Something else here</a>
										<a class="dropdown-item" href="#">Another action</a>
									</div>
								</li> -->
<!--
								<li class="nav-item pl-md-0 ml-0 active">
									<a class="nav-link" href="<?php echo site_url(); ?>">Home</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">LEADERSHIP</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Approach</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">PARTNERSHIPS</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">BLOG</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Contact</a>
								</li>
								<li  class="nav-item pl-md-0 ml-0">
									<a href="" class="nav-link">
										<h2>DOWNLOAD Partnership Info</h2>
									</a>
								</li>
							</ul>
-->
							<?php
								$walker = new Menu_With_Description();
								// echo "<div style='display: none'>";
								wp_nav_menu( 
									array( 
									    'theme_location'	=>	'main-menu', 
									    'container'			=>	'',
									    'menu_class'		=>	'navbar-nav ml-auto py-4 py-md-0',
									    'walker'			=>	$walker 
									) 
								); 
								// echo "</div>";
							?>
						</div>
						
					</nav>		
				</div>
			</div>
		</div>
	</div>


	<!-- mobile menu -->
<div class="modal fade right" id="sidebar-right" tabindex="-1" role="dialog">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
<!-- <h4 class="modal-title">Right Sidebar</h4> -->
</div>
<div class="modal-body p-0">
<div class="topmenu">
							<!-- <ul class="navbar-nav ml-auto py-4 py-md-0">
								<li class="nav-item pl-md-0 ml-0 active">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<a class="dropdown-item" href="#">Something else here</a>
										<a class="dropdown-item" href="#">Another action</a>
									</div>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Portfolio</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Agency</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<a class="dropdown-item" href="#">Something else here</a>
										<a class="dropdown-item" href="#">Another action</a>
									</div>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Journal</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Contact</a>
								</li>
							</ul> -->
							
							<?php
								$walker = new Menu_With_Description();
								// echo "<div style='display: none'>";
								wp_nav_menu( 
									array( 
									    'theme_location'	=>	'main-menu', 
									    'container'			=>	'',
									    'menu_class'		=>	'navbar-nav ml-auto py-4 py-md-0',
									    'walker'			=>	$walker 
									) 
								); 
								// echo "</div>";
							?>
							
							<a href="<?php echo get_template_directory_uri()  ?>/assets/images/The-Acres-Foundation-Educational-Partnership-Brochure.pdf" class="mob-down" targer="_blank" download>
										<h2>DOWNLOAD Partnership Info</h2>
									</a>
						</div>

</div>
</div>
</div>
</div>
