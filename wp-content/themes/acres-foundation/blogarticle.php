<?php /* Template Name: blog article page */ ?>
<?php get_header(); ?>

<section class="comman-cls max-width-ct">
	<div class="container-fluid">
		<div class="row">
			<div>
				<a href="<?php echo site_url('blog'); ?>" class="back-btn">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/leftarrow.svg" alt="" class="img-fluid"><p>Blog</p>
				</a>
			</div>
		</div>
		<div class="row leadership-ct blog-article">
			<div class="col-md-6">
				<h2>How schools can help<br>
students and parents to manage<br>stress in adverse times</h2>
				<div class="Author">
					<p>Author<span>TGAA</span></p>
					<p>Published on<span>August 09, 2020</span></p>
				</div>
			</div>
			<div class="col-md-6 col">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/blogarticlebg.jpg" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row blog-article-dest">
			<div class="col">
			<p>We are currently facing a situation unlike any other and as we watch the world slowly emerge from the pandemic, it’s only natural to have feelings of anxiousness and stress about what the future holds. If we as adults feel this way, then it’s only natural that our children will feel it as well, and they may not possess the same ability to rationalize the current state of the world. In such a situation the mechanisms provided by socio-emotional learning proves crucial for students and parents. It equips students to rationalize, analyze and navigate through difficult and stressful times, something they will carry with them into the workforce and as adults.</p>

<p>So now you can see the importance schools must place on the socio-emotional development of students and how they come into play. The vital role that teachers and schools will now play will be in providing parents with the correct guidance, advice and support to parents in helping students manage their stress.</p>


<p>Talk to them about the current situation</p>

<p>The pandemic has been around long enough, and its effects will continue to be there, both good and bad. Children have felt the effects of this too and their concerns should. Avoiding difficult conversations will only make matters more confusing and stressful for children.</p>

<p>•  Parents should calmly explain the current scenario to children, and work with them through their feelings. By doing so parents will be better preparing them for uncertain situations and how they can cope with it</p>

<p>•  Remember to be truthful in situations where you may not have the answer. Children will appreciate the honesty and learn that it is ok that we don’t always know everything</p>

<p>•  Do not pretend like nothing is wrong. Kids are more intuitive than we think, and they can pick up when things are not right. It’s important to acknowledge and validate their feelings of fear, and let them know that it is ok to have those feelings</p>

<p>•  Balance out the conversations by ensuring to share some good news as well, let them know that it’s not all bad. And most importantly, make sure conversion is continuous. Encourage them to talk to you, communicate with you and make sure to check on their feelings at frequent intervals</p>


<h2>Helping children deal with big and difficult feelings</h2>

<p>We are all going through a whole host of emotions, sometimes it’s almost like a rollercoaster! While it’s completely ok to experience this range of feelings, it’s not good to remain distressed. Staying in a state of distress prevents one from thinking clearly and can also affect ones learning. This is because the brain goes into a state of ‘Fight or Flight’ and the brain is looking for the fastest response but not the best response.</p>

<p>•  To help students work through this, we have to teach them how to first identify their feelings. Teach your children what being in distress means or feels like, and what are the different emotions that could cause it; is it anger, frustration, disappointment etc.</p>

<p>•  Once they have identified the emotions, the next step is to teach them how to cope with it. Give children a few tools to help them manage the emotions, like giving kids a hug to make them feel safe, or deep breathing, slow body movements to help them slow down their mind and emotions.</p>

<p>•  Once they have coped with their big overwhelming feelings, now is the time to problem-solve. Teach them that once they have reached a calmer stage, their minds will be in a better place to think of the best solution and not the fastest solution to the problem.</p>

<p>So now, you can both revisit the problem together and assess what possible solutions you can come up to tackle the problem at hand.</p>


<h2>Proactive tools to make your family feel safe</h2>

<p>Proactive tools or strategies are those which help us take charge of our own decision making and problem-solving. These help us plan for specific issues or scenarios that may arise and be prepared for them.</p>

<p>•  Being prepared and ready will only help children feel more confident and secure when the situation arises as they won’t be lost or confused with how to react.</p>

<p>•  The important proactive tools to equip children with is helping them schedule their day, and to follow a routine. If they know how the day is going to be, it helps them be more prepared both emotionally and mentally.</p>

<p>•  Other important tools for children are adequate exercise, sleep, food and water. Studies prove that children need up to one full hour of exercise in a day which helps with their mental and physical health.</p>

<p>•  Last but not least it’s important for students to stay connected with people, be it family that doesn’t live with you, or their friends who they would otherwise play with at school. Staying connected with their friends is critical to their happiness, as it gives them companionship and helps maintain good mental health.</p>

<p>•  Make sure to celebrate all the birthdays, anniversaries, holidays like you would otherwise, follow patterns that you would outside of this pandemic. By staying connected with the world around them will help students to have a sense of normality through these tough times.</p>


<p>By keeping the above advice in mind parents, teachers and schools for that matter can keep an open line of communication with the children, allowing them to address their feelings and rationally work through them without getting overwhelmed or stressed out about it.</p>

<p>Balancing the socio-emotional development of children, and helping them appropriately navigate through this time give students the skills which they carry with them into adulthood and learn how to stop, analyze, and problem solve whenever they come upon a difficult situation.</p>


<h5>(This article is written by Rohan Parikh, Managing Director of The Green Acres Academy India Today <span>here.</span>)</h5>
		</div>
		</div>
		
	</div>
</section>


<?php get_footer(); ?>