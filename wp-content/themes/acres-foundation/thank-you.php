<?php /* Template Name: thank you page */ ?>
<?php get_header(); ?>



<section>
	<div class="container">
		<div class="row model-school-ct">
			<div class="col  mb-padd-pri">
			<h2 class="text-center m-auto pb-4">Thank you</h2>
			<p>Thanks for contacting us! We will get in touch with you shortly.</p>
			</div>

		</div>
	</div>
</section>

<script>


$(window).on('load', function(){
	window.history.pushState(null, "", window.location.href);        
        window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };
	});
</script>

<?php get_footer(); ?>