<?php /* Template Name: Privacy Policy page */ ?>
<?php get_header(); ?>



<section>
	<div class="container">
		<div class="row model-school-ct mb-padd-pri">
			<h2 class="pl-0 text-center m-auto pb-4 ">The Acres Foundation Privacy Policy</h2>
			<ol class="pl-0">
				<li><p>
					The Acres Foundation (hereinafter referred to as TAF) respects your privacy and is committed to protecting the information you share with us. This statement describes how TAF collects and uses your information to provide services that you request or when you choose to provide us with your personal information. Please read the following to understand TAF privacy practices.</p>
				</li>
				<li><p>
					Personal information means any information that may be used to identify an individual, including, but not limited to, a first and last name, email address, credit card details, a home, postal or other physical address, other contact information, title, occupation, industry, personal interests, client details and other information when needed to provide a service or product or carry out a transaction you have requested.</p>
				</li>
				<li><p>
					When you browse our website, you do so anonymously, unless you have previously registered with us. We do not automatically collect personal information, including your email address. We do log your IP address (the Internet address of your computer) to give us an idea of which part of our website you visit and how long you spend there. But we do not link your IP address to any personal information unless you have registered with us. When personal information is collected, we will inform you at the point of collection the purpose for the collection. TAF will not transfer your personal information to third parties without your prior consent.</p>
				</li>
			</ol>
		</div>
	</div>
</section>
<?php get_footer(); ?>