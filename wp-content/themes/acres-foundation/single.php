<?php
/**
 * Template Name: blog article page 
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Acres_Foundation
 */
get_header();
?>


<section class="comman-cls max-width-ct">
	<div class="container-fluid">
		<div class="row">
			<div>
				<a href="<?php echo site_url('blog'); ?>" class="back-btn">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/leftarrow.svg" alt="" class="img-fluid"><p>Blog</p>
				</a>
			</div>
		</div>

		<?php global $post;
		$author_id = $post->post_author;
		?>

		<div class="row leadership-ct blog-article">
			<div class="col-md-6">
				<h2 class="desk-hd"><?php echo get_the_title() ; ?></h2>
				<div class="Author">
					<p>Author<span><?php echo get_the_author_meta('display_name', $author_id) ; ?></span></p>
					<p>Published on <span><?php echo get_the_date('F j, Y') ; ?></span></p>
				</div>
			</div>
			<div class="col-md-6 col">
				<h2 class="mobile-hd"><?php echo get_the_title() ; ?></h2>
				<?php  echo wp_get_attachment_image(get_post_thumbnail_id(get_the_ID()),'full');?>	
			</div>
		</div>
	</div>
</section>

<section class="leave-commt-style">
	<div class="container">
		<div class="row blog-article-dest">
			<div class="col-12">
				<?php the_content(null, true); ?>
			</div>
		</div>



		<?php $link_arr = get_social_share_links(get_the_ID()); ?>

		<?php

function get_social_share_links($post_id){
	global $post;
    // Get current page URL 
    $sb_url = urlencode(get_permalink($post_id));
 
    // Get current page title
    $sb_title = str_replace( ' ', '%20', get_the_title($post_id));

    $link_arr['whatsapp_url'] = "https://api.whatsapp.com/send?text=" . $sb_url;
    $link_arr['facebook_url'] = "https://www.facebook.com/sharer.php?title=".$sb_title."&url=".$sb_url."&u=".$sb_url;
    $link_arr['twitter_url'] = "http://twitter.com/share?url=".$sb_url;


    // This is the code to share on linkedin 

    $link_arr['linkedin_url'] = "https://www.linkedin.com/sharing/share-offsite/?url={".$sb_url."}" ;

    // $link_arr['whatsapp_url'] = "https://wa.me/?text=".$sb_url;

    // $link_arr['instagram_url'] = "https://www.instagram.com/?url=" . $sb_url;

    return $link_arr;
}

?>

		<div class="row">
			<div class="col-md-7 tags-post-ct">
				<?php $the_post_tags =  get_the_tags(get_the_id()) ; if ( !empty($the_post_tags)) {  ?>
					<?php foreach ($the_post_tags as $tags) { ?>
						<span> <a href="<?php echo site_url('tag/'); echo $tags->slug; ?>"> <?php echo $tags->name ?>  </a></span>
					<?php } ?>						
				<?php } ?>
			</div>
			<div class="col-md-5 share-on-right">
				<div class="share-on">
				<!-- THE ONE FOR SOCIAL MEDIA SHARE ON  -->
					<h3>Share on</h3>
					<div class="social-icon-share">
						<a href="<?php echo $link_arr['facebook_url']; ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/facebook-g.svg"></a>
						<a href="<?php echo $link_arr['twitter_url']; ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/Re-Twitter.png"></a>
						<a href="<?php echo $link_arr['linkedin_url']; ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/linkedin-g.svg"></a>
					</div>
				</div>
			</div>

			<?php // If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		} ?>
		</div>
	</div>
</section>


<?php get_footer(); ?>
