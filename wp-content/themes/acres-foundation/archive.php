<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Acres_Foundation
 */

get_header();

$term = get_queried_object();

$post_tag = $term->slug ; 

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

wp_reset_query();

$args=array(
	'posts_per_page' => 25, 
    'tag' => $post_tag,
    'paged' => $paged
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) { ?>

<section class="comman-cls max-width-ct tag_listing_page">
	
	<div class="container-fluid">
		<h2 id="tag_listing_article_header">ARTICLES FOR TAG <?php echo $post_tag ; ?> </h2>
		<div class="row">
		<?php $count = 0 ;  while ( $the_query->have_posts() ) {
			$count = $count + 1 ;
			$the_query->the_post(); ?>
				<div class="col-md-4 blog-hd">
					<a href="<?php echo get_permalink() ; ?>">
						<?php  echo wp_get_attachment_image(get_post_thumbnail_id(get_the_ID()),'full');?>
					</a>
				</div>
				<div class="col-md-8 blog-hd" style="display: block;">
					<a href="<?php echo get_permalink() ; ?>">
						<h2><?php echo get_the_title() ; ?></h2>
						<p><?php echo wp_trim_words(get_the_excerpt(),25); ?></p>
					</a>
				</div>
		<?php } ?>


		</div>	<!-- class="row" -->
	</div>	<!-- class="container-fluid" -->
</section>	<!-- class="comman-cls max-width-ct" -->

<?php } ?>


<?php get_footer(); ?>