<?php /* Template Name: contact Us page */ ?>
<?php get_header(); ?>
<section class="comman-cls max-width-ct mt-5">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7 contact-us-left-section">
				<h1 class="hightlight-title">Partner with us to build better schools, and a better tomorrow.</h1>
				<div class="w-80">
					<?php
					 if(SITE_ENV == 'dev_server'){	?>
					<div class="contact-form">
						<?php echo gravity_form( 4, false, false, false, '', false ); ?>
					</div>
					<?php }
					else if (SITE_ENV == 'production'){ ?>
						<div class="contact-form">
							<?php echo gravity_form( 7, false, false, false, '', true ); ?>
						</div>
					<?php } ?>
					
					<div class="row mb-5 mt-5">
						<div class="col-md-9 col-lg-7 inner-left-cnt">
							<h3 class="titile-heading-thin mb-4">CONTACT US</h3>
							<h4 class="title-heading-higlight d-none">Prof. Aaditya Lohana</h4>
							<div class="contact-details-sub">
								<a href="mailto:ep.enquiries@acresfoundation.org" class="title-heading-higlight">ep.enquiries@acresfoundation.org</a>
								<a href="tel:+918591201990">+91 85912 01990</a>
								<a href="tel:+918591201991">+91 85912 01991</a>
							</div>
						</div>
						
						<div class="col-md-7 col-lg-5 inner-right-cnt mobile-ct-rig">
							<h3 class="titile-heading-thin mb-4">CONNECT</h3>
							<div class="icon-ct">
								<a href="https://www.facebook.com/theacresfoundation" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/facebook-g.svg"></a>
								<a href="https://www.instagram.com/theacresfoundation/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/instagram-g.svg"></a>
								<a href="https://www.linkedin.com/in/the-acres-foundation-8128931a3" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/linkedin-g.svg"></a>
							</div>
						</div>
						
					</div>
					<div class="row mb-5 pt-3 pb-5">
						<div class="col-md-12">
							<h3 class="titile-heading-thin mb-4">ADDRESS</h3>
							<h4 class="title-heading-higlight">The Acres Foundation</h4>
							<p>411-2/A, Hemu Kalani Marg, <br> Sindhi Society, <br>Chembur, <br>Mumbai - 400 071</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5 contact-us-right-section">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/contact-us-img.svg" alt="">
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>