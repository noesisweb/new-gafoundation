<?php /* Template Name: blog page */ ?>
<?php get_header(); ?>
<section class="max-width-ct">
	<div class="container-fluid pt-4">
		<div class="row">
			<div class="col top-slider blog-pg">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/blog-banner.jpg" alt="" class="img-fluid">
				<div class="sty-ribban">
					<h2>Acres Foundation in news</h2>
					<!-- <p>in global education to India.</p> -->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="purpple-bg">
<div class="max-width-ct">
 <div class="container-fluid">
    <div class="row">
    	<div class="col">
    		<h3>FEATURED ARTICLES</h3>
    	</div>
    </div>
    <div class="row">
    	<div class="col">
		    <div class="swiper-container">
				<div class="swiper-wrapper">
					<?php $featured_articles = get_field('featured_articles') ;
					foreach ( $featured_articles as $the_featured_article ) { 
						?>
					<div class="swiper-slide">
						<div class="card-image">
							<a href="<?php echo get_permalink($the_featured_article->ID) ;?>">
								<?php  echo wp_get_attachment_image(get_post_thumbnail_id($the_featured_article->ID),'full');?>	
								<h2><?php echo $the_featured_article->post_title ; ?></h2>
								<p><?php echo $the_featured_article->post_excerpt ; ?></p>
							</a>
						</div>
					</div>
					<?php } ?>
		      	</div>
		      	<!-- Add Pagination -->
		      	<div class="swiper-pagination dots-ct"></div>
		      	<!-- Add Scrollbar -->
				<!-- <div class="swiper-button-next">
					<i class="fas fa-chevron-circle-right arrow-icon"></i>
				</div>
				<div class="swiper-button-prev">
					<i class="fas fa-chevron-circle-left arrow-icon"></i>
				</div> -->
		    </div>
	    </div>
    </div>
  </div>
  </div>
</section>

<?php 

$post_per_page =  (wp_is_mobile()) ? 6 : 12 ; 

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args=array(
	'post_type' => 'post',
    'posts_per_page' => $post_per_page, 
	'orderby' => 'date',
	'order'   => 'DESC',
    'paged' => $paged
);
$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
?>

<section class="max-width-ct ">
	<div class="container-fluid blog-details-pg">
		<h3>ALL ARTICLES</h3>
		<div class="row blog_listing_all_articles_section">

			<?php while ( $the_query->have_posts() ) {
				$the_query->the_post();
				?>
				<div class="col-md-4 blog-hd mobile-padd">
					<a href="<?php echo get_permalink() ; ?>">
						<?php  echo wp_get_attachment_image(get_post_thumbnail_id(get_the_ID()),'full');?>
						<h2><?php echo get_the_title() ; ?></h2>
						<p><?php echo get_the_excerpt(); ?></p>
					</a>
				</div>
			<?php } ?>
		</div>

		<div class="row blog_listing_load_more_button">
		<?php if ($the_query->max_num_pages>1) { 
			?>
			<div class="col-md-4" id="Load_more_button"  data-total-page="<?php echo $the_query->max_num_pages; ?>" data-next-page-view="2" data-current-page-view="1" >
				<button id="seeMore">Load more</button>
			</div>
		<?php } ?>
		</div>
	</div>
</section>

<?php } ?>



<script>


jQuery('.blog_listing_load_more_button').on('click', '#Load_more_button', function (e) 
{
	e.preventDefault();
	
	var $this = jQuery(this);
	$this.addClass('disabled');
	$this.html('<button id="seeMore">Loading</button>');
	var next_page_view = jQuery(this).attr('data-next-page-view');
	var per_page_view = jQuery(this).attr('data-current-page-view');

	var next_page = parseInt(next_page_view);
	var range = parseInt(jQuery(this).attr('data-total-page'));
	if (next_page <= range) 
	{
		//$this.attr('data-next-page-view',next_page);
		jQuery.ajax({
			url: "./page/" + next_page,
			// data: {
			// 	page: next_page,
			// 	url: window.location.pathname.toString()
			// }
		})
		.done(function (response){
			
			var page_result = $('<div />').append(response).find('.blog_listing_all_articles_section').html();
			$(".blog_listing_all_articles_section").append(page_result);
			$this.html('<button id="seeMore">Load more</button>');
			$this.removeClass('disabled');

			cur_page = next_page;
			next_page = next_page + 1;
			$this.attr("data-next-page-view", next_page);
			$this.attr("data-current-page-view", cur_page);
			if (next_page > range) {
				$this.hide();
			}
		})
		.fail(function () {
			console.log("error");
		});
	}
});

</script>



<?php get_footer(); ?>